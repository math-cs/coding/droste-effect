"""DROSTE EFFECT, by Ehssan
Takes an image with a masked subregion where it creates the Droste effect."""

from PIL import Image


# RGB of masked region
MASK = (255, 255, 255)  # white


def Droste_effect(img_file, num_iter=1):
    with Image.open(img_file) as img:
        img_x, img_y = img.size
        img_aspect_ratio = img_x / img_y
        for _ in range(num_iter):
            subimg_left = 0
            subimg_right = 0
            subimg_top = 0
            subimg_bottom = 0

            # Get the boundary coordinates of the subimage.
            for x in range(img_x):
                for y in range(img_y):
                    if img.getpixel((x, y)) == MASK:
                        if subimg_left == 0 or subimg_left > x:
                            subimg_left = x
                        if subimg_right < x:
                            subimg_right = x
                        if subimg_bottom == 0 or subimg_bottom > y:
                            subimg_bottom = y
                        if subimg_top < y:
                            subimg_top = y
            subimg_x = subimg_right - subimg_left + 1
            subimg_y = subimg_top - subimg_bottom + 1
            subimg_aspect_ratio = subimg_x / subimg_y

            # Resize the image, but preserve the original aspect ratio.
            if img_aspect_ratio < subimg_aspect_ratio:
                resized_img = img.resize((subimg_x, int(
                    subimg_x / img_aspect_ratio)), Image.Dither.NONE)
            else:
                resized_img = img.resize((
                    int(subimg_y * img_aspect_ratio), subimg_y), Image.Dither.NONE)

            # Insert the resized image in the masked region.
            for x in range(subimg_left, subimg_right + 1):
                for y in range(subimg_bottom, subimg_top + 1):
                    if img.getpixel((x, y)) == MASK:
                        pix = resized_img.getpixel(
                            (x - subimg_left, y - subimg_bottom))
                        img.putpixel((x, y), pix)
    return img


Droste_img = Droste_effect('gallery.png', 4)
Droste_img.show()
